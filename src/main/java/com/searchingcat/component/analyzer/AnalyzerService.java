package com.searchingcat.component.analyzer;

import com.searchingcat.entity.*;
import com.searchingcat.parser.procces.AnalyzerInterface;
import com.searchingcat.parser.procces.StoreInterface;
import com.searchingcat.repository.*;
import gorkovoy.lemma.component.CollectLemmaService;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
//import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class AnalyzerService implements AnalyzerInterface
{
    private final PageRepositoryInterface pageRepository;
    private final SiteRepositoryInterface siteRepository;
    private final LemmaRepositoryInterface lemmaRepository;
    private final PageIndexRepositoryInterface pageIndexRepository;
    private final FieldRepositoryInterface fieldRepository;

    private final StoreInterface store;

    private static ArrayList<Field> fields;

    public AnalyzerService(
            PageRepositoryInterface pageRepository,
            SiteRepositoryInterface siteRepository,
            LemmaRepositoryInterface lemmaRepository,
            PageIndexRepositoryInterface pageIndexRepository,
            FieldRepositoryInterface fieldRepository,
            StoreInterface store
    ) {
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
        this.lemmaRepository = lemmaRepository;
        this.pageIndexRepository = pageIndexRepository;
        this.fieldRepository = fieldRepository;
        this.store = store;

        if (fields == null) {
            fields = (ArrayList<Field>) fieldRepository.findAll();
        }
    }

    public Site getSite(String domain)
    {
        return store.getSite(domain);
    }

    public void save(PageKey pageKey, String body, int statusCode)
    {
        if (store.save(pageKey, body, statusCode) && statusCode == 200) {
            //todo move analyze process to consumer
            Page page = this.pageIndexRepository.findByPageKey(pageKey);

            Document doc = Jsoup.parseBodyFragment(page.getContent());
            HashMap<String, Float> ranks = DocumentAnalyzer.run(doc, fields);

            for (String stringLemma : ranks.keySet()) {

                Lemma lemma =this.lemmaRepository.findByLemma(stringLemma);
                if (lemma == null) {
                    lemma = new Lemma(stringLemma);
                }
                lemma.setFrequency(lemma.getFrequency() + 1);
                lemma = this.lemmaRepository.save(lemma);

                PageIndex pageIndex = new PageIndex(pageKey, lemma, ranks.get(stringLemma));
                this.pageIndexRepository.save(pageIndex);
            }
        }
    }
}
