package com.searchingcat.component.analyzer;

import com.searchingcat.entity.Field;
import gorkovoy.lemma.component.CollectLemmaService;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DocumentAnalyzer
{
    static HashMap<String, Float> run(Document doc, ArrayList<Field> fields)
    {
        HashMap<String, HashMap<String, Float>> totalResult = DocumentAnalyzer.handle(doc, fields);
        return DocumentAnalyzer.getRank(totalResult);
    }
    private static HashMap<String, HashMap<String, Float>> handle(Document doc, ArrayList<Field> fields)
    {
        HashMap<String, HashMap<String, Float>> totalResult = new HashMap<>();
        for (Field field : fields) {
            Elements elements = doc.select(field.getSelector());
            for (Element element : elements) {
                List<TextNode> textNodes = element.textNodes();
                for (TextNode textNode : textNodes) {
                    String text = textNode.text();

                    try {
                        CollectLemmaService lemmaService = new CollectLemmaService(new RussianLuceneMorphology());
                        //lemmas
                        HashMap<String, Integer> results = lemmaService.handle(text);

                        HashMap<String, Float> calculatedResults = new HashMap<>();
                        for (String key : results.keySet()) {
                            Integer count = results.get(key);
                            calculatedResults.put(key, count * field.getWeight());
                        }
                        totalResult.put(field.getSelector(), calculatedResults);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return totalResult;
    }

    private static HashMap<String, Float> getRank(HashMap<String, HashMap<String, Float>> totalResult)
    {
        HashMap<String, Float> calculatedRankResult = new HashMap<>();
        for (String key : totalResult.keySet()) {
            HashMap<String, Float> fieldRes = totalResult.get(key);
            for (String keyLemma : fieldRes.keySet()) {
                if (calculatedRankResult.containsKey(keyLemma)) {
                    calculatedRankResult.put(keyLemma, (calculatedRankResult.get(keyLemma) + fieldRes.get(keyLemma)));
                } else {
                    calculatedRankResult.put(keyLemma, fieldRes.get(keyLemma));
                }
            }
        }

        return calculatedRankResult;
    }
}
