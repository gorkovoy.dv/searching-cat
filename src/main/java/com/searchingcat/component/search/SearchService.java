package com.searchingcat.component.search;

import com.searchingcat.repository.LemmaRepositoryInterface;
import com.searchingcat.repository.PageIndexRepositoryInterface;
import com.searchingcat.repository.PageRepositoryInterface;
import com.searchingcat.repository.SiteRepositoryInterface;
import org.springframework.stereotype.Component;

import java.util.HashMap;
@Component
public class SearchService
{
    private final SiteRepositoryInterface siteRepository;
    private final PageRepositoryInterface pageRepository;
    private final LemmaRepositoryInterface lemmaRepository;
    private final PageIndexRepositoryInterface pageIndexRepository;

    public SearchService(
            SiteRepositoryInterface siteRepository,
            PageRepositoryInterface pageRepository,
            LemmaRepositoryInterface lemmaRepository,
            PageIndexRepositoryInterface pageIndexRepository
    ) {
        this.siteRepository = siteRepository;
        this.pageRepository = pageRepository;
        this.lemmaRepository = lemmaRepository;
        this.pageIndexRepository = pageIndexRepository;
    }

    public void searchPages(HashMap<String, Integer> lemmas)
    {
        //todo find lemmas ordered by frequency
        //todo find page-indexes ordered by rank
        //todo find pages in that order
    }
}
