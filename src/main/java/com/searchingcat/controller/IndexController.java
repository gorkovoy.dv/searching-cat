package com.searchingcat.controller;

import com.searchingcat.component.search.SearchService;
import com.searchingcat.parser.Service;
import com.searchingcat.parser.exception.ResultMaxNestedException;
import gorkovoy.lemma.component.CollectLemmaService;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@Controller
public class IndexController
{
    private final Service service;
    private final SearchService searchService;

    public IndexController(
            Service service,
            SearchService searchService
    ) {
        this.service = service;
        this.searchService = searchService;
    }

    @GetMapping("/")
    public String index()
    {
        return "index";
    }
    @RequestMapping(
            value = "/search",
            method = {GET})
    public String search()
    {
        return "search";
    }
    @RequestMapping(
            value = "/search",
            method = {POST})
    public String search(@RequestBody String query) throws IOException
    {
        //todo add form and validation
        //todo move CollectLemmaService to separate class
        CollectLemmaService lemmaService = new CollectLemmaService(new RussianLuceneMorphology());

        query = URLDecoder.decode(query, StandardCharsets.UTF_8.toString());
        System.out.println(query);
        HashMap<String, Integer> results = lemmaService.handle(query);
        System.out.println(results.toString());

        searchService.searchPages(results);

        return "search";
    }

    @GetMapping("/run")
    public String run() throws ResultMaxNestedException, URISyntaxException
    {
        String domain = "izumrudniy.tomsk.ru";
        this.service.run(domain);

        return "index";
    }
}
