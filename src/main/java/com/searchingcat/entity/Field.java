package com.searchingcat.entity;

import javax.persistence.*;

@Entity
@Table(indexes = @Index(name = "page_selector_index", columnList = "selector"))
public class Field
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "field_id")
    private int fieldId;

    @Column(name = "name", columnDefinition = "TEXT", nullable = false)
    private String name;

    @Column(name = "selector", columnDefinition = "TEXT", nullable = false)
    private String selector;

    @Column(name = "weight", columnDefinition = "Decimal(10,2) default '100.00'", nullable = false)
    private String weight;

    public Field()
    {
    }

    public int getFieldId()
    {
        return fieldId;
    }

    public String getName()
    {
        return name;
    }

    public String getSelector()
    {
        return selector;
    }

    public Float getWeight()
    {
        return Float.parseFloat(weight);
    }
}
