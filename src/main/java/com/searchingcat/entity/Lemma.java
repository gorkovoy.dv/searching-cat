package com.searchingcat.entity;

import javax.persistence.*;

@Entity
@Table(indexes = @Index(name = "page_lemma_index", columnList = "lemma"))
public class Lemma
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "lemma_id")
    private int lemmaId;

    @Column(name = "lemma", columnDefinition = "TEXT", nullable = false)
    private String lemma;

    @Column(name = "frequency", columnDefinition = "integer", nullable = false)
    private Integer frequency;

    public Lemma()
    {
    }

    public Lemma(String lemma, Integer frequency)
    {
        this.lemma = lemma;
        this.frequency = frequency;
    }

    public Lemma(String lemma)
    {
        this.lemma = lemma;
        this.frequency = 0;
    }

    public int getLemmaId()
    {
        return lemmaId;
    }

    public String getLemma()
    {
        return lemma;
    }

    public Integer getFrequency()
    {
        return frequency;
    }

    public void setFrequency(Integer frequency)
    {
        this.frequency = frequency;
    }
}
