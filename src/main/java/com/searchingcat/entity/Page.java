package com.searchingcat.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(indexes = @Index(name = "page_path_index", columnList = "path"))
public class Page
{
    @EmbeddedId
    public PageKey pageKey;

    @Column(name = "code", columnDefinition = "BIGINT", nullable = false)
    private int code;

    @Column(name = "content", columnDefinition = "TEXT", nullable = false)
    private String content;

    @OneToMany
    @JoinColumns({
        @JoinColumn(name="site_id", referencedColumnName="site_id"),
        @JoinColumn(name="path", referencedColumnName="path")
    })
    private Set<PageIndex> indexSet;

    public Page()
    {
    }

    public Page(PageKey id, int code, String content)
    {
        this.pageKey = id;
        this.code = code;
        this.content = content;
    }

    public PageKey getPageKey()
    {
        return pageKey;
    }

    public String getContent()
    {
        return content;
    }
}
