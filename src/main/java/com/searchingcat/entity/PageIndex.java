package com.searchingcat.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Index")
public class PageIndex implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "page_index_id")
    private int pageIndexId;

    public PageKey pageKey;

    @OneToOne
    @JoinColumn(name = "lemma_id")
    private Lemma lemma;

    @Column(name = "rank", columnDefinition = "Decimal(10,2) default '100.00'", nullable = false)
    private double rank;

    public PageIndex()
    {
    }

    public PageIndex(PageKey pageKey, Lemma lemma, double rank)
    {
        this.pageKey = pageKey;
        this.lemma = lemma;
        this.rank = rank;
    }
}
