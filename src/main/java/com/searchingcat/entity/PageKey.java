package com.searchingcat.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class PageKey implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "site_id")
    private Site siteId;

    @Column(name = "path", columnDefinition = "TEXT", nullable = false)
    private String path;

    public PageKey()
    {
    }

    public PageKey(Site siteId, String path)
    {
        this.siteId = siteId;
        this.path = path;
    }

    public Site getSiteId()
    {
        return siteId;
    }

    public String getPath()
    {
        return path;
    }
}
