package com.searchingcat.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(indexes = @Index(name = "site_url_index", columnList = "url"))
public class Site
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int site_id;

    @Column(name = "name", columnDefinition = "TEXT", nullable = true)
    private String name;

    @Column(name = "url", columnDefinition = "TEXT", nullable = false)
    private String url;

    @Column(name = "status", columnDefinition = "TEXT", nullable = true)
    private String status;

    @Column(name = "status_time", columnDefinition = "TIMESTAMP", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date status_time;

    @Column(name = "last_error", columnDefinition = "TEXT", nullable = true)
    private String last_error;

    public Site()
    {
    }

    public Site(String url)
    {
        this.url = url;
    }

    public int getSiteId()
    {
        return site_id;
    }

    public String getUrl()
    {
        return url;
    }
}
