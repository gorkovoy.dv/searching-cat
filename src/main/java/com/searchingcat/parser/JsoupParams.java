package com.searchingcat.parser;

import com.searchingcat.parser.procces.ParamsInterface;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JsoupParams implements ParamsInterface
{
    @Value("${parser.userAgent}")
    private String userAgent;

    @Value("${parser.referrer}")
    private String referrer;

    public String getUserAgent()
    {
        return userAgent;
    }

    public String getReferrer()
    {
        return referrer;
    }
}
