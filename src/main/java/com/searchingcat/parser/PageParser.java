package com.searchingcat.parser;

import com.searchingcat.entity.PageKey;
import com.searchingcat.entity.Site;
import com.searchingcat.parser.entity.Link;
import com.searchingcat.parser.entity.PageParserResponse;
import com.searchingcat.parser.procces.ParamsInterface;
import com.searchingcat.parser.procces.ScannedStoreInterface;
import com.searchingcat.parser.procces.SiteParserInterface;
import com.searchingcat.parser.procces.StoreInterface;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@Component
public class PageParser implements SiteParserInterface
{
//    private final StoreInterface store;

    private Link link;
    private Site site;
    private ScannedStoreInterface scannedStore;

    private ParamsInterface params;

    public PageParser(ScannedStoreInterface scannedStore, ParamsInterface params)
    {
        this.params = params;
        this.scannedStore = scannedStore;
    }

    public SiteParserInterface init(Site site) throws URISyntaxException
    {
        this.link = new Link(site.getUrl());
        this.site = site;

        return this;
    }

    public SiteParserInterface initByLink(Site site, Link link) throws URISyntaxException
    {
        this.link = link;
        this.site = site;

        return this;
    }

    public PageParserResponse getPage() throws InterruptedException
    {
        if (this.scannedStore.isScanned(link) || this.scannedStore.isScanning(link)) {
            return new PageParserResponse();
        }

        this.scannedStore.addToScanning(link);
        Thread.sleep(120);

        try {
            Connection.Response response = Jsoup.connect(link.toString()).userAgent(params.getUserAgent()).referrer(params.getReferrer()).execute();
            return new PageParserResponse(site, link, response, response.parse());

        } catch (IOException exception) {
            // do nothing

            if (exception instanceof UnsupportedMimeTypeException || exception instanceof UnknownHostException) {
                this.scannedStore.addToInvalidLinks(link);
            } else if (exception instanceof MalformedURLException) {
                System.out.println(link.toString());
                exception.printStackTrace();
            } else {
                exception.printStackTrace();
            }
        } finally {
            this.scannedStore.addToScanned(link);
            this.scannedStore.removeFromScanning(link);
        }


        return new PageParserResponse();
    }

    public Map<String, Link> getLinks(PageParserResponse pageParserResponse)
    {
        Map<String, Link> linkMap = new HashMap();

      //  try {
            Document doc = pageParserResponse.getDocument();
            if (doc.body() != null) {
                Elements links = doc.body().getElementsByTag("a");
                for (Element link : links) {
                    Link lineEntity = new Link(link.attr("href").trim(), this.link.getDomain());
                    if (lineEntity.isValid() && !this.scannedStore.isScanned(lineEntity)) {
                        linkMap.put(lineEntity.getPath(), lineEntity);
                    }
                }
            }
//        } catch (IOException exception) {
//            // do nothing
//
//            if (exception instanceof UnsupportedMimeTypeException || exception instanceof UnknownHostException) {
//                this.scannedStore.addToInvalidLinks(link);
//            } else {
//                exception.printStackTrace();
//            }
//        }
        return linkMap;
    }

//    public Map<String, Link> run() throws InterruptedException
//    {
//        Map<String, Link> linkMap = new HashMap();
//
//        if (this.scannedStore.isScanned(link) || this.scannedStore.isScanning(link)) {
//            return linkMap;
//        }
//
//        this.scannedStore.addToScanning(link);
//        Thread.sleep(120);
//
//        try {
//            Connection.Response response = Jsoup.connect(link.toString()).userAgent(params.getUserAgent()).referrer(params.getReferrer()).execute();
//
//            Document doc = response.parse();
//            Link linkToSave = new Link(link.toString(), link.getDomain());
//            PageKey pageKey = new PageKey(site, linkToSave.getPath());
//            if (store.save(pageKey, doc.toString(), response.statusCode())) {
//                // new AnalyzerService.run(pageKey)
//            }
//
//            if (doc.body() != null) {
//                Elements links = doc.body().getElementsByTag("a");
//                for (Element link : links) {
//                    Link lineEntity = new Link(link.attr("href").trim(), this.link.getDomain());
//                    if (lineEntity.isValid() && !this.scannedStore.isScanned(lineEntity)) {
//                        linkMap.put(lineEntity.getPath(), lineEntity);
//                    }
//                }
//            }
//        } catch (IOException exception) {
//            // do nothing
//
//            if (exception instanceof UnsupportedMimeTypeException || exception instanceof UnknownHostException) {
//                this.scannedStore.addToInvalidLinks(link);
//            } else {
//                exception.printStackTrace();
//            }
//        } finally {
//            this.scannedStore.addToScanned(link);
//            this.scannedStore.removeFromScanning(link);
//        }
//
//        return linkMap;
//    }

    public SiteParserInterface getNewInstance(Link link) throws URISyntaxException
    {
        return new PageParser(scannedStore, this.params).initByLink(site, link);
    }

    public ScannedStoreInterface getScannedStore()
    {
        return scannedStore;
    }
}
