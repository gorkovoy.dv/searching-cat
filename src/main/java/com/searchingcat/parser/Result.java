package com.searchingcat.parser;

import com.searchingcat.parser.exception.ResultMaxNestedException;

import java.util.HashMap;

public class Result<S, R extends Result> extends HashMap
{
    private int maxNestedLevel = 2;
    private int nestedLevel = 0;

    public Result(int maxNestedLevel)
    {
        this.maxNestedLevel = maxNestedLevel;
    }

    public Result<String, Result> getNext(String uri) throws ResultMaxNestedException
    {
        if (isMaxNested()) {
            throw new ResultMaxNestedException(String.format("max nested level is %s; %s must be created", maxNestedLevel, getNextNested()));
        }

        Result res = new Result(maxNestedLevel);
        res.maxNestedLevel = maxNestedLevel;
        res.nestedLevel = getNextNested();

        this.put(uri, res);

        return res;
    }

    public boolean isMaxNested()
    {
        return getNextNested() > maxNestedLevel;
    }

    private int getNextNested()
    {
        return this.nestedLevel + 1;
    }
}
