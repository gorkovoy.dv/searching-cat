package com.searchingcat.parser;

import com.searchingcat.parser.entity.Link;
import com.searchingcat.parser.procces.ScannedStoreInterface;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class ScannedPages implements ScannedStoreInterface
{
    private volatile  Set<Link> scanned = new HashSet<Link>();
    private volatile  Set<Link> scanning = new HashSet<Link>();
    private volatile  Set<Link> invalidLinks = new HashSet<Link>();

    public synchronized boolean isScanned(Link link)
    {
        return scanned.contains(link);
    }

    public void addToScanned(Link link)
    {
        scanned.add(link);
    }

    public Set<Link> getScannedPages()
    {
        return scanned;
    }

    public synchronized boolean isScanning(Link link)
    {
        return scanning.contains(link);
    }

    public void addToScanning(Link link)
    {
        scanning.add(link);
    }

    public void removeFromScanning(Link link)
    {
        scanning.remove(link);
    }

    public void addToInvalidLinks(Link link)
    {
        invalidLinks.add(link);
    }

    public Set<Link> getInvalidLinks()
    {
        return invalidLinks;
    }
}
