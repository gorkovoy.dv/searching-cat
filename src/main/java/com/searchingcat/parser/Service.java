package com.searchingcat.parser;

import com.searchingcat.component.analyzer.AnalyzerService;
import com.searchingcat.parser.exception.ResultMaxNestedException;
import com.searchingcat.parser.procces.AnalyzerInterface;
import com.searchingcat.parser.procces.RecursiveTaskLinkParser;
import com.searchingcat.parser.procces.SiteParserInterface;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.net.URISyntaxException;
import java.util.concurrent.ForkJoinPool;

@Component
public class Service
{
    @Value("${application.parser.maxNestedLevel}")
    private Integer maxNestedLevel;

    private final SiteParserInterface pageParser;
    private final AnalyzerInterface analyzerService;

    public Service(
            SiteParserInterface pageParser,
            AnalyzerInterface analyzerService
    ) {
        this.pageParser = pageParser;
        this.analyzerService = analyzerService;

    }

    public void run(String domain) throws URISyntaxException, ResultMaxNestedException
    {
        Result result = new Result(maxNestedLevel);

        RecursiveTaskLinkParser parser = new RecursiveTaskLinkParser(
                pageParser.init(analyzerService.getSite(domain)),
                result.getNext(domain),
                analyzerService
        );
        new ForkJoinPool().invoke(parser);

        System.out.println("===============================SCANNED PAGES===============================");
        pageParser.getScannedStore().getScannedPages().forEach(System.out::println);
        System.out.println("===============================INVALID LINKS===============================");
        pageParser.getScannedStore().getInvalidLinks().forEach(System.out::println);
    }
}
