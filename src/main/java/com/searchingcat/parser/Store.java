package com.searchingcat.parser;

import com.searchingcat.entity.Page;
import com.searchingcat.entity.PageKey;
import com.searchingcat.entity.Site;
import com.searchingcat.parser.procces.StoreInterface;
import com.searchingcat.repository.PageRepositoryInterface;
import com.searchingcat.repository.SiteRepositoryInterface;
import org.springframework.stereotype.Component;

@Component
public class Store implements StoreInterface
{
    private final PageRepositoryInterface pageRepository;
    private final SiteRepositoryInterface siteRepository;

    public Store(
            PageRepositoryInterface pageRepository,
            SiteRepositoryInterface siteRepository
    ) {
        this.pageRepository = pageRepository;
        this.siteRepository = siteRepository;
    }

    public boolean save(PageKey pageKey, String body, int statusCode)
    {
        Page item = pageRepository.findByPageKey(pageKey);
        if (item == null) {
            Page page = new Page(pageKey, statusCode, body);
            pageRepository.save(page);

            return true;
        }

        return false;
    }

    @Override
    public Site getSite(String domain)
    {
        Site site = siteRepository.findByUrl(domain);

        if (site == null) {
            site = new Site(domain);
            siteRepository.save(site);
        }

        return site;
    }
}
