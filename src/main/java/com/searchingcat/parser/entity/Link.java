package com.searchingcat.parser.entity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.regex.Pattern;

public class Link
{
    private boolean alienLink = false;
    private String path;
    private String domain;
    protected static final String FIRST = "/";

    private URI uriDomain;
    private URI uriByUrl;

    public Link(String url, String domain)
    {
        url = normalizeUrl(url);

        try {
            uriByUrl = new URI(url);

            this.path = uriByUrl.getPath().equals("") ? FIRST : uriByUrl.getPath();
        } catch (URISyntaxException exception) {
            this.path = url;
            alienLink = false;
        }

        try {
            if (!domain.contains("://")) {
                domain = "http://" + domain;
            }

            uriDomain = new URI(domain);
            this.domain = normalizeDomain(uriDomain.getHost());
        } catch (URISyntaxException ignored) {

        }

        if (uriByUrl != null && uriDomain.getHost() != null && uriByUrl.getHost() != null) {
            alienLink = !normalizeDomain(uriByUrl.getHost()).equals(normalizeDomain(uriDomain.getHost()));
        }
    }

    public Link(String url) throws URISyntaxException
    {
        url = normalizeUrl(url);

        uriByUrl = new URI(url);
        this.path = uriByUrl.getPath().equals("") ? FIRST : uriByUrl.getPath();

        this.domain = normalizeDomain(uriByUrl.getHost());
    }

    public boolean isValid()
    {
        if (path.length() < 1 || alienLink) {
            return false;
        }

        String pattern = "[www.]{4}" + domain +"|[//]{2}" + domain;
        Pattern r = Pattern.compile(pattern);

        return !path.matches("#");
    }

    public String getPath()
    {
        return path;
    }

    public String getDomain()
    {
        return domain;
    }

    @Override
    public String toString()
    {
        if (uriByUrl != null && uriByUrl.getHost() != null) {
            return uriByUrl.toString();
        }

        return new StringBuilder().append("http")
                .append("://")
                .append(normalizeDomain(uriDomain.getHost()))
                .append(path)
                .toString();
    }

    private String normalizeDomain(String domain)
    {
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }
    private String normalizeUrl(String url)
    {
        if(!url.contains("://")) {
            url = "http://" + url;
        }

        return url;
    }
    @Override
    public boolean equals(Object link)
    {
        return this.toString().equals(link.toString());
    }
    @Override
    public int hashCode() {
        return Objects.hash(this.toString());
    }
}
