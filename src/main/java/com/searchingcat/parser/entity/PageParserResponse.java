package com.searchingcat.parser.entity;

import com.searchingcat.entity.Site;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import java.util.HashMap;
import java.util.Map;

public class PageParserResponse
{
    private Map<String, Link> linkMap = new HashMap();
    private Site site;
    private Link currentLink;
    private Response response;
    private Document document;

    public PageParserResponse(Site site, Link currentLink, Response response, Document document)
    {
        this.site = site;
        this.currentLink = currentLink;
        this.response = response;
        this.document = document;
    }

    public PageParserResponse()
    {
    }

    public boolean isEmpty()
    {
        return response == null;
    }

    public Response getResponse()
    {
        return response;
    }

    public Document getDocument()
    {
        return document;
    }

    public Link getCurrentLink()
    {
        return currentLink;
    }

    public Site getSite()
    {
        return site;
    }
}
