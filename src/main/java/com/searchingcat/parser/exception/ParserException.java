package com.searchingcat.parser.exception;

public class ParserException extends Exception
{
    public ParserException(String message)
    {
        super(message);
    }
}
