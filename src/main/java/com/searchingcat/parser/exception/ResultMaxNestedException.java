package com.searchingcat.parser.exception;

public class ResultMaxNestedException extends Exception
{
    public ResultMaxNestedException(String message)
    {
        super(message);
    }
}
