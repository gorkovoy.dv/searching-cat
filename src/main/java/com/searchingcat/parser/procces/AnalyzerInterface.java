package com.searchingcat.parser.procces;

import com.searchingcat.entity.PageKey;
import com.searchingcat.entity.Site;

public interface AnalyzerInterface
{
    public Site getSite(String domain);
    public void save(PageKey pageKey, String body, int statusCode);

}
