package com.searchingcat.parser.procces;

public interface ParamsInterface
{
    public String getUserAgent();
    public String getReferrer();
}
