package com.searchingcat.parser.procces;

import com.searchingcat.component.analyzer.AnalyzerService;
import com.searchingcat.entity.PageKey;
import com.searchingcat.parser.Result;
import com.searchingcat.parser.entity.Link;
import com.searchingcat.parser.entity.PageParserResponse;
import com.searchingcat.parser.exception.ParserException;
import com.searchingcat.parser.exception.ResultMaxNestedException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveAction;


public class RecursiveTaskLinkParser extends RecursiveAction
{
    private final AnalyzerInterface analyzerService;
    private SiteParserInterface parseEntity;
    private Result result;

    public RecursiveTaskLinkParser(SiteParserInterface parseEntity, Result result, AnalyzerInterface analyzerService)
    {
        this.parseEntity = parseEntity;
        this.result = result;
        this.analyzerService = analyzerService;
    }

    @Override
    protected void compute()
    {
        try {
            List<RecursiveTaskLinkParser> threads = new ArrayList<>();
//            Map<String, Link> map = parseEntity.run();


            PageParserResponse pageParserResponse = parseEntity.getPage();

            if (!pageParserResponse.isEmpty()) {
                //todo
                Link linkToSave = new Link(
                        pageParserResponse.getCurrentLink().toString(),
                        pageParserResponse.getCurrentLink().getDomain()
                );
                PageKey pageKey = new PageKey(pageParserResponse.getSite(), linkToSave.getPath());


                analyzerService.save(pageKey, pageParserResponse.getDocument().toString(), pageParserResponse.getResponse().statusCode());
//                if (store.save(pageKey, pageParserResponse.getDocument().toString(), pageParserResponse.getResponse().statusCode())) {
//                    // new AnalyzerService.run(pageKey)
//                }

                Map<String, Link> map = parseEntity.getLinks(pageParserResponse);

                map.forEach((str, link) -> {
                    if (!result.isMaxNested()) {
                        try {
                            RecursiveTaskLinkParser parser = new RecursiveTaskLinkParser(
                                    parseEntity.getNewInstance(link),
                                    result.getNext(link.getPath()),
                                    analyzerService
                            );
                            parser.fork();
                            threads.add(parser);
                        } catch (ResultMaxNestedException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                });

                for (RecursiveTaskLinkParser thread : threads) {
                    thread.join();
                }
            }
        } catch (InterruptedException  exception) {
            exception.printStackTrace();
        }
    }
}
