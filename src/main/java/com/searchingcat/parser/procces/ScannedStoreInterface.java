package com.searchingcat.parser.procces;

import com.searchingcat.parser.entity.Link;

import java.util.Set;

public interface ScannedStoreInterface
{
    public boolean isScanned(Link link);

    public void addToScanned(Link link);

    public Set<Link> getScannedPages();

    public boolean isScanning(Link link);

    public void addToScanning(Link link);

    public void removeFromScanning(Link link);

    public void addToInvalidLinks(Link link);

    public Set<Link> getInvalidLinks();
}
