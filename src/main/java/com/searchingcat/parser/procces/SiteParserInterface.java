package com.searchingcat.parser.procces;

import com.searchingcat.entity.Site;
import com.searchingcat.parser.entity.Link;
import com.searchingcat.parser.entity.PageParserResponse;
import com.searchingcat.parser.exception.ParserException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

public interface SiteParserInterface
{
//    public Map<String, Link> run() throws InterruptedException;
    public PageParserResponse getPage() throws InterruptedException;
    public Map<String, Link> getLinks(PageParserResponse pageParserResponse);
    public SiteParserInterface getNewInstance(Link link) throws URISyntaxException;
    public SiteParserInterface init(Site site) throws URISyntaxException;

    public ScannedStoreInterface getScannedStore();
}
