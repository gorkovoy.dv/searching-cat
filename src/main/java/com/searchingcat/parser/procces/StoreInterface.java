package com.searchingcat.parser.procces;

import com.searchingcat.entity.PageKey;
import com.searchingcat.entity.Site;

public interface StoreInterface
{
    public boolean save(PageKey pageKey, String body, int statusCode);
    public Site getSite(String domain);
}
