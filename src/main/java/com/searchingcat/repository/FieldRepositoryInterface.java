package com.searchingcat.repository;

import com.searchingcat.entity.Field;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FieldRepositoryInterface extends CrudRepository<Field, Integer>
{

}
