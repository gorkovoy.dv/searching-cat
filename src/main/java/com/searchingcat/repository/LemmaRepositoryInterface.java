package com.searchingcat.repository;

import com.searchingcat.entity.Lemma;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LemmaRepositoryInterface extends CrudRepository<Lemma, Integer>
{
    @Query("select l from Lemma l where l.lemma=?1")
    public Lemma findByLemma(String lemma);
}
