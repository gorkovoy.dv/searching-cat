package com.searchingcat.repository;

import com.searchingcat.entity.Page;
import com.searchingcat.entity.PageKey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepositoryInterface extends CrudRepository<Page, Integer>
{
    @Query("select p from Page p where p.pageKey.path=?1")
    public Page findByPath(String path);

    @Query("select p from Page p where p.pageKey=?1")
    public Page findByPageKey(PageKey pk);
}
