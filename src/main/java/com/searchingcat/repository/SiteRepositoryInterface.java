package com.searchingcat.repository;

import com.searchingcat.entity.Site;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteRepositoryInterface extends CrudRepository<Site, Integer>
{
    @Query("select s from Site s where s.url=?1")
    public Site findByUrl(String url);
}
