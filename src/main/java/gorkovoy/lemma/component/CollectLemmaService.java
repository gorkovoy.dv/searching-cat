package gorkovoy.lemma.component;

import org.apache.lucene.morphology.MorphologyImpl;
import gorkovoy.lemma.component.lemma.LemmaFilter;
import gorkovoy.lemma.component.sentence.SentenceTransformer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CollectLemmaService
{
    private MorphologyImpl luceneMorph;

    public CollectLemmaService(MorphologyImpl luceneMorph)
    {
        this.luceneMorph = luceneMorph;
    }

    public HashMap<String, Integer> handle (String sentence) throws IOException
    {
        System.out.println(sentence);

        List<String> words = SentenceTransformer.handle(sentence);
        ArrayList<String> allLemmas = new ArrayList<>();
        for(String item : words) {
            System.out.println(item);
            List<String> wordMorphInfoForms = this.luceneMorph.getMorphInfo(item);
            allLemmas.addAll(wordMorphInfoForms);
        }

        LemmaFilter lemmaFilter = new LemmaFilter(allLemmas);
        HashMap<String, Integer> results = lemmaFilter.filter().getResult();

        return results;
    }
}
