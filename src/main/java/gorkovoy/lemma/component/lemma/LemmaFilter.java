package gorkovoy.lemma.component.lemma;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LemmaFilter
{
    private List<String> lemmas;
    private List<String> result;

    public LemmaFilter(List<String> lemmas)
    {
        this.lemmas = lemmas;
    }

    public LemmaFilter filter()
    {
        List<String> result = new ArrayList<>();

        for(String item : lemmas) {
            String[] part = item.split("\\|");
            if (part.length >= 2) {
                Pattern pattern = Pattern.compile("n СОЮЗ|o МЕЖД|l ПРЕДЛ");
                Matcher matcher = pattern.matcher(part[1]);

                if (!matcher.find()) {
                    result.add(part[0]);
                }
            }
        }

        this.result = result;
        return this;
    }

    public HashMap<String, Integer> getResult()
    {
        HashMap<String, Integer> result = new HashMap<String, Integer>();
        for (String item : this.result) {
            if (result.containsKey(item)) {
                result.put(item, result.get(item) + 1);
            } else {
                result.put(item, 1);
            }
        }

        return result;
    }
}
