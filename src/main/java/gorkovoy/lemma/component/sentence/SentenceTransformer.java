package gorkovoy.lemma.component.sentence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SentenceTransformer
{
    public static List<String> handle(String sentence)
    {
        List<String> result = new ArrayList<>();
        String[] sentenceArray = sentence
                .toLowerCase(Locale.ROOT)
                .replaceAll("[^а-яё]", " ")
                .split(" ");
        sentenceArray = Arrays.stream(sentenceArray).filter((String item) -> {
            item = item.replaceAll(" ", "");
            if (item.equals("")) {
                return false;
            }

            return true;
        }).toArray(String[]::new);
        result.addAll(Arrays.asList(sentenceArray));

        return result;
    }
}
