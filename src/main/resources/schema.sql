--works with spring.datasource.initialization-mode=always

-- Drop table

-- DROP TABLE public.field;

CREATE TABLE IF NOT EXISTS public.field (
	field_id int4 NOT NULL,
	"name" text NOT NULL,
	selector text NOT NULL,
	weight numeric(10,2) NOT NULL DEFAULT 100.00,
	CONSTRAINT field_pkey PRIMARY KEY (field_id)
);
CREATE INDEX IF NOT EXISTS page_selector_index ON public.field USING btree (selector);

-- Drop table

-- DROP TABLE public.lemma;

CREATE TABLE IF NOT EXISTS public.lemma (
	lemma_id int4 NOT NULL,
	frequency int4 NOT NULL,
	lemma text NOT NULL,
	CONSTRAINT lemma_pkey PRIMARY KEY (lemma_id)
);
CREATE INDEX IF NOT EXISTS page_lemma_index ON public.lemma USING btree (lemma);

-- Drop table

-- DROP TABLE public.site;

CREATE TABLE IF NOT EXISTS public.site (
	site_id int4 NOT NULL,
	last_error text NULL,
	"name" text NULL,
	status text NULL,
	status_time timestamp NULL,
	url text NOT NULL,
	CONSTRAINT site_pkey PRIMARY KEY (site_id)
);
CREATE INDEX IF NOT EXISTS site_url_index ON public.site USING btree (url);

-- Drop table

-- DROP TABLE public.page;

CREATE TABLE IF NOT EXISTS public.page (
	"path" text NOT NULL,
	code int8 NOT NULL,
	"content" text NOT NULL,
	site_id int4 NOT NULL,
	CONSTRAINT page_pkey PRIMARY KEY (path, site_id),
	CONSTRAINT fkj2jx0gqa4h7wg8ls0k3y221h2 FOREIGN KEY (site_id) REFERENCES site(site_id)
);
CREATE INDEX IF NOT EXISTS page_path_index ON public.page USING btree (path);

-- Drop table

-- DROP TABLE public."index";

CREATE TABLE IF NOT EXISTS public."index" (
	"path" text NOT NULL,
	page_index_id int4 NOT NULL,
	"rank" numeric(10,2) NOT NULL DEFAULT 100.00,
	site_id int4 NOT NULL,
	lemma_id int4 NULL,
	CONSTRAINT index_pkey PRIMARY KEY (path, site_id, page_index_id),
	CONSTRAINT fk710f52lodi1s36spjr590499t FOREIGN KEY (lemma_id) REFERENCES lemma(lemma_id),
	CONSTRAINT fkbp04m2iti4xv483gauypy5edx FOREIGN KEY (site_id) REFERENCES site(site_id),
	CONSTRAINT fksqls6cxi9dk17wfgy85ovqbx0 FOREIGN KEY (path, site_id) REFERENCES page(path, site_id)
);