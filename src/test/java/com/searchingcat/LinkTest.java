package com.searchingcat;

import com.searchingcat.parser.exception.ResultMaxNestedException;
import junit.framework.TestCase;
import com.searchingcat.parser.entity.Link;
import com.searchingcat.parser.Result;

import java.net.URISyntaxException;


public class LinkTest extends TestCase
{
    public void testValidLink() throws URISyntaxException
    {
        Link link = new Link("lenta.ru/news", "lenta.ru");
        Link link2 = new Link("www.lenta.ru/news", "www.lenta.ru");
        Link link3 = new Link("/news1", "www.lenta.ru");
        Link link4 = new Link("lenta.ru/news", "http://lenta.ru");

        assertEquals(true, link.isValid());
        assertEquals(true, link2.isValid());
        assertEquals(true, link3.isValid());
        assertEquals(true, link4.isValid());
    }

    public void testInvalidLink() throws URISyntaxException
    {
        Link link2 = new Link("http://moslenta.ru/news", "http://lenta.ru");
        Link link3 = new Link("http://mos.lenta.ru/news", "http://lenta.ru");
        Link link4 = new Link("http://insta.ru/lenta.ru", "http://www.lenta.ru");
        Link link5 = new Link("https://www.youtube.com/channel/UCfqU-kKXq868D6d4fy8fhkA?utm_source=lentasocialbuttons&utm_campaign=youtube", "http://www.lenta.ru");
        
        assertEquals(false, link2.isValid());
        assertEquals(false, link3.isValid());
        assertEquals(false, link4.isValid());
        assertEquals(false, link5.isValid());
    }

    public void testWithDomainLink() throws URISyntaxException
    {
        Link link = new Link("/rubrics/russia/", "http://lenta.ru");
        Link link2 = new Link("http://lenta.ru/news", "http://lenta.ru");

        assertEquals("http://lenta.ru/rubrics/russia/", link.toString());
        assertEquals("http://lenta.ru/news", link2.toString());

    }
}
