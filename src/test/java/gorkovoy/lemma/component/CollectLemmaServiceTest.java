package gorkovoy.lemma.component;
import junit.framework.TestCase;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;

import java.io.IOException;
import java.util.HashMap;

public class CollectLemmaServiceTest extends TestCase
{
    public void testCollectLemmaService() throws IOException
    {
        String sentence = "и Повторное появление леопарда в Осетии позволяет   предположить, что леопард постоянно обитает в некоторых районах Северного Кавказа.";

        CollectLemmaService lemmaService = new CollectLemmaService(new RussianLuceneMorphology());
        HashMap<String, Integer> results = lemmaService.handle(sentence);

//        results.forEach((item, count) -> System.out.println(String.format("%s - %s", item, count)));

        assertEquals("{повторный=1, некоторый=1, появление=1, постоянно=1, постоянный=1, некоторые=1, позволять=1, предположить=1, северный=1, район=1, кавказ=1, осетия=1, леопард=2, обитать=1}", results.toString());
    }


    public void testEmptyCollectLemmaService() throws IOException
    {
        String sentence = "";

        CollectLemmaService lemmaService = new CollectLemmaService(new RussianLuceneMorphology());
        HashMap<String, Integer> results = lemmaService.handle(sentence);

        assertEquals("{}", results.toString());
    }

    public void testWrongSymbolsCollectLemmaService() throws IOException
    {
        String sentence = "Спасибо за подписку! qwerty \n кот 12 - ; ^^ %%";

        CollectLemmaService lemmaService = new CollectLemmaService(new RussianLuceneMorphology());
        HashMap<String, Integer> results = lemmaService.handle(sentence);

        assertEquals("{спасибо=1, подписка=1, кот=1}", results.toString());
    }
}
